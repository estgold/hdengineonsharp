﻿using HDEngine.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Tao.DevIl;
using Tao.FreeGlut;
using Tao.OpenGl;

namespace HDEngine
{
    public partial class MainFrame : Form
    {
        // START Game Objects

        private Person person = new Person();

        // END Game Objects

        private int imageId = 0;

        private uint mGlTextureObject = 0;

        private int rot = 0;

        private string err = "";

        private Point mainFrameCenter;

        private const float PIPower = 180f / (float)Math.PI;

        public MainFrame()
        {
            InitializeComponent();
            AnT.InitializeContexts();
            mainFrameCenter = new Point(this.Width / 2, this.Height / 2);
        }

        private void MainFrame_Load(object sender, EventArgs e)
        {
            // инициализация бибилиотеки glut
            Glut.glutInit();
            // инициализация режима экрана
            Glut.glutInitDisplayMode(Glut.GLUT_RGB | Glut.GLUT_DOUBLE);

            // инициализация библиотеки OpenIL
            Il.ilInit();
            Il.ilEnable(Il.IL_ORIGIN_SET);

            // установка цвета очистки экрана (RGBA)
            Gl.glClearColor(255, 255, 255, 1);

            // установка порта вывода
            Gl.glViewport(0, 0, AnT.Width, AnT.Height);

            // активация проекционной матрицы
            Gl.glMatrixMode(Gl.GL_PROJECTION);
            // очистка матрицы
            Gl.glLoadIdentity();

            // установка перспективы
            Glu.gluPerspective(30, AnT.Width / AnT.Height, 1, 100);

            // установка объектно-видовой матрицы
            Gl.glMatrixMode(Gl.GL_MODELVIEW);
            Gl.glLoadIdentity();

            // начальные настройки OpenGL
            Gl.glEnable(Gl.GL_DEPTH_TEST);
            Gl.glEnable(Gl.GL_LIGHTING);
            Gl.glEnable(Gl.GL_LIGHT0);

            Gl.glEnable(Gl.GL_BLEND);
            Gl.glBlendFunc(Gl.GL_ONE, Gl.GL_ONE_MINUS_SRC_ALPHA);

            Gl.glAlphaFunc(Gl.GL_GREATER, 0.8f);




            openTexture();









            // активация таймера
            renderTimer.Start();

        }

        private void Draw()
        {
            person.Go();
            // увеличиваем угол поворота
            rot++;
            // корректируем угол
            if (rot > 360)
                rot = 0;

            //person.lookAtVector.addVectorCoords(0.01f,0);

            // очистка буфера цвета и буфера глубины
            Gl.glClear(Gl.GL_COLOR_BUFFER_BIT | Gl.GL_DEPTH_BUFFER_BIT);
            Gl.glClearColor(255, 255, 255, 1);


            // включаем режим текстурирования
            Gl.glEnable(Gl.GL_TEXTURE_2D);
            // включаем режим текстурирования, указывая идентификатор mGlTextureObject
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, mGlTextureObject);

            // очищение текущей матрицы
            Gl.glLoadIdentity();

            Gl.glRotated(person.lookAtVector.p.y*PIPower, 1, 0, 0); // X
            Gl.glRotated(person.lookAtVector.p.x*PIPower, 0, 1, 0); // Y
            //Gl.glRotated(-person.lookAtVector.p.z* PIPower, 0, 0, 1); // Z




            ////Skybox
            //Gl.glDisable(Gl.GL_DEPTH_TEST);
            
            //// сохраняем состояние матрицы
            //Gl.glPushMatrix();

            //// выполняем перемещение для более наглядного представления сцены
            //Gl.glTranslated(0, -0.5, -5);
            //// реализуем поворот объекта
            //Gl.glRotated(rot, 0, 1, 0);

            //drawObject();
            //// возвращаем матрицу
            //Gl.glPopMatrix();

            //Gl.glEnable(Gl.GL_DEPTH_TEST);





            // очищение текущей матрицы
            Gl.glLoadIdentity();

            //Gl.glEnable(Gl.GL_ALPHA_TEST);

            //// сохраняем состояние матрицы
            //Gl.glPushMatrix();

            //// выполняем перемещение для более наглядного представления сцены
            //Gl.glTranslated(0, -0.5, -5);
            //// реализуем поворот объекта
            //Gl.glRotated(rot, 0, 1, 0);

            //drawObject();
            //// возвращаем матрицу
            //Gl.glPopMatrix();


            //Gl.glDisable(Gl.GL_ALPHA_TEST);

            //Glu.gluLookAt(person.position.x, person.position.y + person.height, person.position.z,
            //person.lookAt.x,
            //person.lookAt.y + person.height,
            //person.lookAt.z,
            //0, 1, 0);

            Glu.gluLookAt(
                person.position.x, person.position.y + person.height, person.position.z,
                // точка, куда смотрит чувак. Должна расчитываться как место, где стоит чувак, плюс вектор направления взгляда
                person.lookAtVector.p.x + person.position.x, // X
                person.lookAtVector.p.y + person.height + person.position.y, // Y
                person.lookAtVector.p.z + person.position.z, // Z
                0, 1, 0);

            // сохраняем состояние матрицы
            Gl.glPushMatrix();

            // выполняем перемещение для более наглядного представления сцены
            Gl.glTranslated(0, -0.5, -5);
            // реализуем поворот объекта
            Gl.glRotated(rot, 0, 1, 0);

            drawObject();
            // возвращаем матрицу
            Gl.glPopMatrix();
            // сохраняем состояние матрицы
            Gl.glPushMatrix();

            // выполняем перемещение для более наглядного представления сцены
            Gl.glTranslated(0, -0.5, -5);
            // реализуем поворот объекта
            Gl.glRotated(-rot, 0, 1, 0);

            drawObject();

            // возвращаем матрицу
            Gl.glPopMatrix();
            // отключаем режим текстурирования
            Gl.glDisable(Gl.GL_TEXTURE_2D);


            // обновляем элемент со сценой
            AnT.Invalidate();
        }

        private  void drawObject()
        {
            // отрисовываем полигон
            Gl.glBegin(Gl.GL_QUADS);

            // указываем поочередно вершины и текстурные координаты
            Gl.glVertex3d(1, 1, 0);

            Gl.glTexCoord2f(0, 0);

            Gl.glVertex3d(1, -1, 0);

            Gl.glTexCoord2f(1, 0);

            Gl.glVertex3d(-1, -1, 0);

            Gl.glTexCoord2f(1, 1);

            Gl.glVertex3d(-1, 1, 0);

            Gl.glTexCoord2f(0, 1);

            // завершаем отрисовку
            Gl.glEnd();

        }


        private void openTexture()
        {

            // создаем изображение с идентификатором imageId
            Il.ilGenImages(1, out imageId);
            // делаем изображение текущим
            Il.ilBindImage(imageId);

            // адрес изображения, полученный с помощью окна выбра файла
            string url = "C:\\Users\\kolimator\\source\\repos\\HDEngine\\HDEngine\\bin\\Debug\\Resources\\Skybox256.png";
            //string url = "C:\\Users\\kolimator\\source\\repos\\Suharnikov_PA_PRIm116_lab5\\Suharnikov_PA_PRIm116_lab5\\bin\\Debug\\SHD Logo.jpg";

            // пробуем загрузить изображение
            if (Il.ilLoadImage(url))
            {

                // если загрузка прошла успешно
                // сохраняем размеры изображения
                int width = Il.ilGetInteger(Il.IL_IMAGE_WIDTH);
                int height = Il.ilGetInteger(Il.IL_IMAGE_HEIGHT);

                // определяем число бит на пиксель
                int bitspp = Il.ilGetInteger(Il.IL_IMAGE_BITS_PER_PIXEL);

                switch (bitspp) // в зависимости от полученного результата
                {

                    // создаем текстуру, используя режим GL_RGB или GL_RGBA
                    case 24:
                        mGlTextureObject = MakeGlTexture(Gl.GL_RGB, Il.ilGetData(), width, height);
                        break;
                    case 32:
                        mGlTextureObject = MakeGlTexture(Gl.GL_RGBA, Il.ilGetData(), width, height);
                        break;

                }
                
                // очищаем память
                Il.ilDeleteImages(1, ref imageId);

            }
        }


        // создание текстуры в памяти openGL
        private static uint MakeGlTexture(int Format, IntPtr pixels, int w, int h)
        {

            // идентификатор текстурного объекта
            uint texObject;

            // генерируем текстурный объект
            Gl.glGenTextures(1, out texObject);

            // устанавливаем режим упаковки пикселей
            Gl.glPixelStorei(Gl.GL_UNPACK_ALIGNMENT, 1);

            // создаем привязку к только что созданной текстуре
            Gl.glBindTexture(Gl.GL_TEXTURE_2D, texObject);

            // устанавливаем режим фильтрации и повторения текстуры
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_S, Gl.GL_REPEAT);
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_WRAP_T, Gl.GL_REPEAT);
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MAG_FILTER, Gl.GL_LINEAR);
            Gl.glTexParameteri(Gl.GL_TEXTURE_2D, Gl.GL_TEXTURE_MIN_FILTER, Gl.GL_LINEAR);
            Gl.glTexEnvf(Gl.GL_TEXTURE_ENV, Gl.GL_TEXTURE_ENV_MODE, Gl.GL_REPLACE);

            // создаем RGB или RGBA текстуру
            switch (Format)
            {

                case Gl.GL_RGB:
                    Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGB, w, h, 0, Gl.GL_RGB, Gl.GL_UNSIGNED_BYTE, pixels);
                    break;

                case Gl.GL_RGBA:
                    Gl.glTexImage2D(Gl.GL_TEXTURE_2D, 0, Gl.GL_RGBA, w, h, 0, Gl.GL_RGBA, Gl.GL_UNSIGNED_BYTE, pixels);
                    break;

            }

            // возвращаем идентификатор текстурного объекта

            return texObject;

        }

        private void renderTimer_Tick(object sender, EventArgs e)
        {
            Draw();
            labelDebug.Text = "Person new XY: " + person.lookAtVector.alignXY * 180f / (float) Math.PI + " XZ: " + person.lookAtVector.alignXZ * 180f / (float)Math.PI;
            labelDebug.Text += "\nPerson old X: " + person.lookAtVector.p.x + " Y: " + person.lookAtVector.p.y + " Z:"+ person.lookAtVector.p.z;
        }

        private void MainFrame_KeyDown(object sender, KeyEventArgs e)
        {

            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.W:
                    person.MoveForward();
                    break;
                case Keys.S:
                    person.MoveBack();
                    break;
                case Keys.D:
                    person.StrafeRight();
                    break;
                case Keys.A:
                    person.StrafeLeft();
                    break;
            }

        }

        private void MainFrame_MouseMove(object sender, MouseEventArgs e)
        {
            //labelDebug.Text ="Mouse: "+ e.X + ":"+e.Y;

            //person.AddLookAt(mainFrameCenter.X - e.X, mainFrameCenter.Y - e.Y);
            person.lookAtVector.addVectorCoordsByDergee(mainFrameCenter.X - e.X, mainFrameCenter.Y - e.Y);
            
            Cursor.Position = this.PointToScreen(mainFrameCenter);
            
        }

        private void MainFrame_KeyUp(object sender, KeyEventArgs e)
        {
            switch (e.KeyCode)
            {
                case Keys.Escape:
                    this.Close();
                    break;
                case Keys.W:
                case Keys.S:
                    person.MoveNone();
                    break;
                case Keys.D:
                case Keys.A:
                    person.StrafeNone();
                    break;
            }
        }
    }
}
