﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDEngine
{
    static class Program
    {
        /// <summary>
        /// Главная точка входа для приложения.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            bool toClose = false;
            List<string> argList = new List<string>();

            foreach (string arg in args)
            {
                switch (arg)
                {
                    case "-s":
                    case "--settings":

                        Settings settings = new Settings();
                        Application.Run(settings);
                        toClose = settings.DialogResult != DialogResult.OK; // закрывается, если в настройках было выбрано не OK
                        break;
                }
            }

            if (!toClose)
            {
                Application.Run(new MainFrame());
            }
        }
    }
}
