﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HDEngine
{
    public partial class Settings : Form
    {
        BindingList<SettButt> buttonsList = new BindingList<SettButt>();

        public Settings()
        {
            InitializeComponent();
        }

        private void buttonSaveLoad_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.OK;
            this.Close();
        }

        private void buttonExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void buttonSaveExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            MessageBoxButtons mbb = MessageBoxButtons.OK;
            DialogResult dr = new DialogResult();
            string msg = "HDEngine on C#"+
                "Сухарников Пётр,"+
                "ВлГУ, 2018";
            string cap = "Информация об авторах";

            MessageBox.Show(msg, cap, mbb);

        }

        private void Settings_Load(object sender, EventArgs e)
        {
            buttonsList.Add(new SettButt("Test", 'a'));
            dataGridView1.DataSource = buttonsList;

            comboBoxGraphic.SelectedIndex = 0;
            comboBoxQuality.SelectedIndex = 0;
        }
    }

    public class SettButt
    {
        public string action { get; set; }
        public char button { get; set; }


        public SettButt(string action, char button)
        {
            this.action = action;
            this.button = button;
        }

    }

}
