﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDEngine.Engine
{
    /// <summary>
    /// Единичный вектор в трёхмерном пространстве
    /// 
    /// Полезно использовать для указания направления.
    /// Кроме того предоставляет данные о градусах направления
    /// на плоскостях XY и XZ в радианах. Последнее позволяет сэкономить
    /// время для некоторых задач, например перемещения пользователя
    /// в пространстве физически обычно требует знать его направление только
    /// на плоскости XZ, независимо от того, в какую точку в плоскости XY
    /// он смотрит.
    /// </summary>
    class Vector3D
    {
        //http://www.portalero.ru/Matematika/mat/2240.htm

        public Point3D p;
        // Углы в радианах
        public float alignXY;
        public float alignXZ;

        private float cosXY; //cos0
        private float cosXZ; //cosф
        private float sinXY; //sin0
        private float sinXZ; //sinф

        private float maxAlignXY = 180f;
        private float minAlignXY = -180f;
        private bool transAlignXY = true;
        private float maxAlignXZ = 180f;
        private float minAlignXZ = -180f;
        private bool transAlignXZ = true;

        public Vector3D()
        {
            p = new Point3D(1, 0, 0);
        }

        public Vector3D(float alignXY, float alignXZ)
        {
            p = new Point3D();
            setVectorCoords(alignXY, alignXZ);
        }

        public Vector3D(float alignXY, float alignXZ, float maxAlignXY, float minAlignXY, bool transAlignXY, float maxAlignXZ, float minAlignXZ, bool transAlignXZ) : this(alignXY, alignXZ)
        {
            this.maxAlignXY = maxAlignXY;
            this.minAlignXY = minAlignXY;
            this.transAlignXY = transAlignXY;
            this.maxAlignXZ = maxAlignXZ;
            this.minAlignXZ = minAlignXZ;
            this.transAlignXZ = transAlignXZ;
        }

        public void setAlignXYLimits(float maxAlignXY, float minAlignXY, bool transAlignXY)
        {
            this.maxAlignXY = maxAlignXY;
            this.minAlignXY = minAlignXY;
            this.transAlignXY = transAlignXY;
        }

        public void setAlignXYAlignZYLimits(float maxAlignZY, float minAlignZY, bool transAlignZY)
        {
            this.maxAlignXZ = maxAlignZY;
            this.minAlignXZ = minAlignZY;
            this.transAlignXZ = transAlignZY;
        }

        /// <summary>
        /// Рассчитывает координаты по полученным углам,
        /// полностью переписывая старое значение направления.
        /// Значения углов должны быть в градусах
        /// 
        /// </summary>
        /// <param name="alignXYDegree"></param>
        /// <param name="alignXZDegree"></param>
        public void setVectorCoordsByDergee(float alignXYDegree, float alignXZDegree)
        {
            float radPow = (float)Math.PI / 180f;
            setVectorCoords(alignXYDegree*radPow, alignXZDegree * radPow);
        }

        /// <summary>
        /// Рассчитывает координаты по полученным углам,
        /// полностью переписывая старое значение направления.
        /// Значения углов должны быть в радианах
        /// 
        /// </summary>
        /// <param name="alignXY"></param>
        /// <param name="alignXZ"></param>
        public void setVectorCoords(float alignXY, float alignXZ)
        {
            ParseCorrectAligns(ref alignXY, ref alignXZ);

            this.alignXY = alignXY;
            this.alignXZ = alignXZ;

            cosXY = (float)Math.Cos(alignXY);
            cosXZ = (float)Math.Cos(alignXZ);
            sinXY = (float)Math.Sin(alignXY);
            sinXZ = (float)Math.Sin(alignXZ);

            p.x = cosXY * cosXZ;
            p.y = cosXY * sinXY;
            p.z = sinXZ;
        }

        /// <summary>
        /// Проверяет, не выходит ли полученный градус за допустимый предел.
        /// Если выходит, то проверяет, доступен ли для этого угла переход на
        /// противоположное значение.
        /// 
        /// Если да -- выполняет расчеты по перемещению градуса с одной границы на другую
        /// 
        /// Если нет -- то просто устанавливает самое близкое значение ограничений
        /// </summary>
        /// <param name="alignXY"></param>
        /// <param name="alignXZ"></param>
        private void ParseCorrectAligns(ref float alignXY, ref float alignXZ)
        {
            //Parse by XY
            if (alignXY > maxAlignXY)
            {
                alignXY = (transAlignXY) ? alignXY - maxAlignXY + Math.Abs(minAlignXY) : maxAlignXY;
                // типа если больше нужного значения, то добавляет его со стороны меньшего
            }
            else if (alignXY < minAlignXY)
            {
                alignXY = (transAlignXY) ? Math.Abs(maxAlignXY) - alignXY - maxAlignXY  : minAlignXY;
                // типа если меньше нужного значения, то отрезает его со стороны большего
            }
            //Parse by XZ
            if (alignXZ > maxAlignXZ)
            {
                alignXZ = (transAlignXZ) ? alignXZ - maxAlignXZ + Math.Abs(minAlignXZ) : maxAlignXZ;
                // типа если больше нужного значения, то добавляет его со стороны меньшего
            }
            else if (alignXZ < minAlignXZ)
            {
                alignXZ = (transAlignXZ) ? Math.Abs(maxAlignXZ) - alignXZ - maxAlignXZ : minAlignXZ;
                // типа если меньше нужного значения, то отрезает его со стороны большего
            }
        }


        /// <summary>
        /// Рассчитывает координаты по полученным углам,
        /// добавляя полученные значения к старым.
        /// Значения углов должны быть в градусах
        /// 
        /// </summary>
        /// <param name="alignXYDegree"></param>
        /// <param name="alignXZDegree"></param>
        public void addVectorCoordsByDergee(float alignXYDegree, float alignXZDegree)
        {
            float radPow = (float)Math.PI / 180f;
            addVectorCoords(alignXYDegree * radPow, alignXZDegree * radPow);
        }

        /// <summary>
        /// Рассчитывает координаты по полученным углам,
        /// добавляя полученные значения к старым.
        /// Значения углов должны быть в радианах
        /// 
        /// </summary>
        /// <param name="alignXY"></param>
        /// <param name="alignXZ"></param>
        public void addVectorCoords(float alignXY, float alignXZ)
        {
            //alignXY += this.alignXY;
            //alignXZ += this.alignXZ;

            setVectorCoords(alignXY + this.alignXY, alignXZ + this.alignXZ);
        }

        // TODO создать методы, нормализирующие входные Point
        // Для одного Point - направление на Point относительно [0,0,0]
        // Для двух Point - направление от первого ко второму

    }
}
