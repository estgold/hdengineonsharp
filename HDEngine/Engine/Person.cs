﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDEngine.Engine
{
    /// <summary>
    /// Информация о юзвере, где он сейчас находится, как идёт, куда
    /// идёт, куда смотрит и какую воду пьёт.
    /// 
    /// Информация о пользователе есть информация о камере (она содержится
    /// здесь), однако имеет дополнительные поля, не имеющие отношеня
    /// непосредственно к камере
    /// </summary>
    class Person
    {

        // Движение
        private int move = 0; // +1 - движение вперёд; -1 - движение назад
        private int strafe = 0; // +1 - стрейф вправо; -1 - стрейф влево
        private float moveSpeed = 0.2f; // скорость ходьбы (для движения и стрефа одинавого)
        private float moveRotate = 0.05f; // скорость движения мышкой

        // Положение
        public Point3D position = new Point3D();
        public Point3D lookAt = new Point3D();
        public float height = 0; // высота персонажа
        public float lookAtAngleX = 0;
        public float lookAtAngleY = 0;
        public Vector3D lookAtVector;

        public Person()
        {
            lookAt.y = 1;
            lookAtVector = new Vector3D(0, 0, 90, -90, false, 180, -180, true);
            lookAtVector.setVectorCoordsByDergee(5, -90);
        }

        public Person(Point3D position, Point3D lookAt, int height)
        {
            this.position = position;
            this.lookAt = lookAt;
            this.height = height;
        }

        //public Person(float positionX, float positionY, float positionZ, float lookAtX, float lookAtY, float lookAtZ, int height)
        //{
        //    this.position = new Point3D(positionX,positionY,positionZ);
        //    this.lookAt = new Point3D(lookAtX,lookAtY,lookAtZ);
        //    this.height = height;
        //}

        public void MoveForward() { move = 1; }

        public void MoveNone() { move = 0; }

        public void MoveBack() { move = -1; }

        public void StrafeRight() { strafe = 1; }

        public void StrafeNone() { strafe = 0; }

        public void StrafeLeft() { strafe = -1; }

        public void AddLookAt(float x, float y) // получает параметры мыши и изменяет точку взгляда персонажа
        {
            x = lookAtAngleX + x * moveRotate;
            if (x < 0) { x = 360f+x; } // меньше миниума = 
            if (x > 360f) { x = x-360f; } // больше максиума = 

            y = lookAtAngleY + y * moveRotate;
            if (y < -89.0) { y = -89.0f; }
            if (y > 89.0) { y = 89.0f; }

            lookAtAngleX = x;
            lookAtAngleY = y;
            updateLookAt();
        }

        private void updateLookAt()
        {
            lookAt.x = position.x - (float)Math.Sin(lookAtAngleX / 180 * Math.PI);
            lookAt.y = position.y + (float)Math.Tan(lookAtAngleY / 180 * Math.PI);
            lookAt.z = position.z - (float)Math.Cos(lookAtAngleX / 180 * Math.PI);
        }

        public void Go()
        {
            if (move != 0 || strafe != 0)
            {
                position.x += moveSpeed * move;
                position.z += moveSpeed * strafe;
                updateLookAt();
            }
        }



    }
}
