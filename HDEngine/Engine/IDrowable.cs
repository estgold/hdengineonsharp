﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDEngine.Engine
{
    interface IDrowable
    {
        void Draw(Person ui);
    }
}
