﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDEngine.Engine
{
    class Location : IDrowable
    {
        /// <summary>
        /// Хранит информацио об объектах и скайбоксе на локации
        /// 
        /// </summary>
        private Skybox skybox = null;



        public Location()
        {

        }

        public void Draw(Person ui)
        {
            if (skybox != null) { skybox.Draw(ui); }
        }
    }
}
