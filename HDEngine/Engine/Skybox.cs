﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HDEngine.Engine
{
    /// <summary>
    /// Хранит информацию о текстуре окружения.
    /// Может быть Null - тогда рендер скайбокса на локации не будет хранится.
    /// 
    /// Скайбокс отличается от всех остальных графических объектов тем,
    /// что не является физическим, никак не влияет на другие объекты и всегда находится
    /// на максимальном удалении, доступном при выбранном рендеренге.
    /// Каждая локация может иметь свой скайбокс
    /// 
    /// TODO: определиться с тем, как загружается текстура
    /// </summary>
    class Skybox : IDrowable
    {


        public void Draw(Person ui)
        {
            throw new NotImplementedException();
        }
    }
}
